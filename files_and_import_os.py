#John Wichita
#12-5-2021
#Bellevue University
#Python programming with Prof. Mack
#files and import os

import os
print('this program is designed to ask you for a directory, test to see if that directory exists and then ask you for to name a file in that directory that will contain your name, address, and phone number')

dirName=input("please enter the directory you would like to save your file in: ")
try:
    os.mkdir(dirName)
except FileExistsError:
    print(f"{dirName} already exists")

fileName=input('what is the name of the file you would like to create?: ')
path=os.path.join(dirName, fileName)

enterName=input("please enter your name: ")
enterAddress=input("please enter your address: ")
enterPhone=input("please enter your phone number: ")

with open(path, 'w') as file_ob:
    file_ob.write(f'{enterName},{enterAddress},{enterPhone}')

